//
// Created by phuctm97 on 2/23/2018.
//

#ifndef __YCSI_H__
#define __YCSI_H__

#define YCSI_CUU "\e[%dA"
#define YCSI_CUD "\e[%dB"
#define YCSI_CUF "\e[%dC"
#define YCSI_CUB "\e[%dD"
#define YCSI_CNL "\e[%dE"
#define YCSI_CPL "\e[%dF"
#define YCSI_CHA "\e[%dG"
#define YCSI_CUP "\e[%d;%dH"
#define YCSI_ED "\e[%dJ"
#define YCSI_EL "\e[%dK"
#define YCSI_SU "\e[%dS"
#define YCSI_SD "\e[%dT"
#define YCSI_HVP "\e[%d;%df"
#define YCSI_SGR "\e[%sm"
#define YCSI_DSR "\e[6n"
#define YCSI_SCP "\e[s"
#define YCSI_RCP "\e[u"
#define YCSI_25H "\e[25h"
#define YCSI_25L "\e[25l"

#define YCSI_CURSOR_UP YCSI_CUU
#define YCSI_CURSOR_DOWN YCSI_CUD
#define YCSI_CURSOR_FORWARD YCSI_CUF
#define YCSI_CURSOR_BACK YCSI_CUB
#define YCSI_CURSOR_NEXT_LINE YCSI_CNL
#define YCSI_CURSOR_PREVIOUS_LINE YCSI_CPL
#define YCSI_CURSOR_HORIZONTAL_ABSOLUTE YCSI_CHA
#define YCSI_CURSOR_POSITION YCSI_CUP
#define YCSI_ERASE_DISPLAY YCSI_ED
#define YCSI_ERASE_LINE YCSI_EL
#define YCSI_ERASE_CURRENT_TO_END 0
#define YCSI_ERASE_CURRENT_TO_BEGIN 1
#define YCSI_ERASE_BEGIN_TO_END 2
#define YCSI_SCROLL_UP YCSI_SU
#define YCSI_SCROLL_DOWN YCSI_SD
#define YCSI_HORIZONTAL_VERTICAL_POSITION YCSI_HVP
#define YCSI_SELECT_GRAPHICS_RENDITION YCSI_SGR
#define YCSI_DEVICE_STATUS_REPORT YCSI_DSR
#define YCSI_SAVE_CURSOR_POSITION YCSI_SCP
#define YCSI_RESTORE_CURSOR_POSITION YCSI_RCP
#define YCSI_SHOW_CURSOR YCSI_25H
#define YCSI_HIDE_CURSOR YCSI_25L
#define YCSI_ERASE_TO_END 0
#define YCSI_ERASE_TO_BEGIN 1
#define YCSI_ERASE_ENTIRE 2

extern int ycsi_supported;

char *ycsi_cuu(char *s, int n);

char *ycsi_cud(char *s, int n);

char *ycsi_cuf(char *s, int n);

char *ycsi_cub(char *s, int n);

char *ycsi_cnl(char *s, int n);

char *ycsi_cpl(char *s, int n);

char *ycsi_cha(char *s, int n);

char *ycsi_cup(char *s, int n, int m);

char *ycsi_ed(char *s, int n);

char *ycsi_el(char *s, int n);

char *ycsi_su(char *s, int n);

char *ycsi_sd(char *s, int n);

char *ycsi_hvp(char *s, int n, int m);

char *ycsi_sgr(char *s, const char *sgr);

char *ycsi_dsr(char *s);

char *ycsi_scp(char *s);

char *ycsi_rcp(char *s);

char *ycsi_25h(char *s);

char *ycsi_25l(char *s);

char *ycsi_cursor_up(char *s, int n);

char *ycsi_cursor_down(char *s, int n);

char *ycsi_cursor_forward(char *s, int n);

char *ycsi_cursor_back(char *s, int n);

char *ycsi_cursor_next_line(char *s, int n);

char *ycsi_cursor_previous_line(char *s, int n);

char *ycsi_cursor_horizontal_absolute(char *s, int n);

char *ycsi_cursor_position(char *s, int n, int m);

char *ycsi_erase_display(char *s, int n);

char *ycsi_erase_line(char *s, int n);

char *ycsi_scroll_up(char *s, int n);

char *ycsi_scroll_down(char *s, int n);

char *ycsi_horizontal_vertical_position(char *s, int n, int m);

char *ycsi_select_graphics_rendition(char *s, const char *sgr);

char *ycsi_device_status_report(char *s);

char *ycsi_save_cursor_position(char *s);

char *ycsi_restore_cursor_position(char *s);

char *ycsi_show_cursor(char *s);

char *ycsi_hide_cursor(char *s);


#define YCSI_SGR_RESET 0
#define YCSI_SGR_HIGH_INTENSITY 1
#define YCSI_SGR_LOW_INTENSITY 2
#define YCSI_SGR_ITALIC 3
#define YCSI_SGR_UNDERLINE 4
#define YCSI_SGR_SLOW_BLINK 5
#define YCSI_SGR_RAPID_BLINK 6
#define YCSI_SGR_REVERSE 7
#define YCSI_SGR_CONCEAL 8
#define YCSI_SGR_CROSSED_OUT 9
#define YCSI_SGR_PRIMARY_FONT 10
#define YCSI_SGR_ALTERNATIVE_FONT_1 11
#define YCSI_SGR_ALTERNATIVE_FONT_2 12
#define YCSI_SGR_ALTERNATIVE_FONT_3 13
#define YCSI_SGR_ALTERNATIVE_FONT_4 14
#define YCSI_SGR_ALTERNATIVE_FONT_5 15
#define YCSI_SGR_ALTERNATIVE_FONT_6 16
#define YCSI_SGR_ALTERNATIVE_FONT_7 17
#define YCSI_SGR_ALTERNATIVE_FONT_8 18
#define YCSI_SGR_ALTERNATIVE_FONT_9 19
#define YCSI_SGR_FRAKTUR 20
#define YCSI_SGR_BOLD_OFF_OR_DOUBLE_UNDERLINE 21
#define YCSI_SGR_NORMAL_INTENSITY 22
#define YCSI_SGR_NOT_ITALIC_NOT_FRAKTUR 23
#define YCSI_SGR_UNDERLINE_OFF 24
#define YCSI_SGR_BLINK_OFF 25
#define YCSI_SGR_INVERSE_OFF 27
#define YCSI_SGR_CONCEAL_OFF 28
#define YCSI_SGR_NOT_CROSSED_OUT 29
#define YCSI_SGR_FG_BLACK 30
#define YCSI_SGR_FG_RED 31
#define YCSI_SGR_FG_GREEN 32
#define YCSI_SGR_FG_YELLOW 33
#define YCSI_SGR_FG_BLUE 34
#define YCSI_SGR_FG_MAGENTA 35
#define YCSI_SGR_FG_CYAN 36
#define YCSI_SGR_FG_WHITE 37
#define YCSI_SGR_FG 38
#define YCSI_SGR_FG_DEFAULT 39
#define YCSI_SGR_BG_BLACK 40
#define YCSI_SGR_BG_RED 41
#define YCSI_SGR_BG_GREEN 42
#define YCSI_SGR_BG_YELLOW 43
#define YCSI_SGR_BG_BLUE 44
#define YCSI_SGR_BG_MAGENTA 45
#define YCSI_SGR_BG_CYAN 46
#define YCSI_SGR_BG_WHITE 47
#define YCSI_SGR_BG 48
#define YCSI_SGR_BG_DEFAULT 49
#define YCSI_SGR_FG_BRIGHT_BLACK 90
#define YCSI_SGR_FG_BRIGHT_RED 91
#define YCSI_SGR_FG_BRIGHT_GREEN 92
#define YCSI_SGR_FG_BRIGHT_YELLOW 93
#define YCSI_SGR_FG_BRIGHT_BLUE 94
#define YCSI_SGR_FG_BRIGHT_MAGENTA 95
#define YCSI_SGR_FG_BRIGHT_CYAN 96
#define YCSI_SGR_FG_BRIGHT_WHITE 97
#define YCSI_SGR_BG_BRIGHT_BLACK 100
#define YCSI_SGR_BG_BRIGHT_RED 101
#define YCSI_SGR_BG_BRIGHT_GREEN 102
#define YCSI_SGR_BG_BRIGHT_YELLOW 103
#define YCSI_SGR_BG_BRIGHT_BLUE 104
#define YCSI_SGR_BG_BRIGHT_MAGENTA 105
#define YCSI_SGR_BG_BRIGHT_CYAN 106
#define YCSI_SGR_BG_BRIGHT_WHITE 107
#define YCSI_SGR_FRAMED 51
#define YCSI_SGR_ENCIRCLED 52
#define YCSI_SGR_OVERLINED 53
#define YCSI_SGR_NOT_FRAMED_NOT_ENCIRCLED 54
#define YCSI_SGR_NOT_OVERLINED 55

char *ycsi_build_sgr(char *s, int n, ...);

char *ycsi_reset_sgr(char *s);

void ycsi_apply(const char *s);

void ycsi_apply_and_reset(const char *s);

#endif //__YCSI_H__
