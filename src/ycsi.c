//
// Created by phuctm97 on 2/23/2018.
//

#include <stdio.h>
#include <stdarg.h>
#include "../include/ycsi.h"

int ycsi_supported = 1;

char *ycsi_cuu(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CUU, n);
    return s;
}

char *ycsi_cud(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CUD, n);
    return s;
}

char *ycsi_cuf(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CUF, n);
    return s;
}

char *ycsi_cub(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CUB, n);
    return s;
}

char *ycsi_cnl(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CNL, n);
    return s;
}

char *ycsi_cpl(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CPL, n);
    return s;
}

char *ycsi_cha(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CHA, n);
    return s;
}

char *ycsi_cup(char *s, int n, int m) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CUP, n, m);
    return s;
}

char *ycsi_ed(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_ED, n);
    return s;
}

char *ycsi_el(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_EL, n);
    return s;
}

char *ycsi_su(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_SU, n);
    return s;
}

char *ycsi_sd(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_SD, n);
    return s;
}

char *ycsi_hvp(char *s, int n, int m) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_HVP, n, m);
    return s;
}

char *ycsi_sgr(char *s, const char *sgr) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_SGR, sgr);
    return s;
}

char *ycsi_dsr(char *s) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_DSR);
    return s;
}

char *ycsi_scp(char *s) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_SCP);
    return s;
}

char *ycsi_rcp(char *s) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_RCP);
    return s;
}

char *ycsi_25h(char *s) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_25H);
    return s;
}

char *ycsi_25l(char *s) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_25L);
    return s;
}

char *ycsi_cursor_up(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CUU, n);
    return s;
}

char *ycsi_cursor_down(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CUD, n);
    return s;
}

char *ycsi_cursor_forward(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CUF, n);
    return s;
}

char *ycsi_cursor_back(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CUB, n);
    return s;
}

char *ycsi_cursor_next_line(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CNL, n);
    return s;
}

char *ycsi_cursor_previous_line(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CPL, n);
    return s;
}

char *ycsi_cursor_horizontal_absolute(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CHA, n);
    return s;
}

char *ycsi_cursor_position(char *s, int n, int m) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_CUP, n, m);
    return s;
}

char *ycsi_erase_display(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_ED, n);
    return s;
}

char *ycsi_erase_line(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_EL, n);
    return s;
}

char *ycsi_scroll_up(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_SU, n);
    return s;
}

char *ycsi_scroll_down(char *s, int n) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_SD, n);
    return s;
}

char *ycsi_horizontal_vertical_position(char *s, int n, int m) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_HVP, n, m);
    return s;
}

char *ycsi_select_graphics_rendition(char *s, const char *sgr) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_SGR, sgr);
    return s;
}

char *ycsi_device_status_report(char *s) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_DSR);
    return s;
}

char *ycsi_save_cursor_position(char *s) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_SCP);
    return s;
}

char *ycsi_restore_cursor_position(char *s) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_RCP);
    return s;
}

char *ycsi_show_cursor(char *s) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_25H);
    return s;
}

char *ycsi_hide_cursor(char *s) {
    if (!ycsi_supported) return s;
    sprintf(s, YCSI_25L);
    return s;
}

char *ycsi_build_sgr(char *s, int n, ...) {
    if (n <= 0)return s;

    char fmt[255];
    const int fmt_len = n * 3;

    for (int i = 0; i < fmt_len; i += 3) {
        fmt[i] = '%';
        fmt[i + 1] = 'd';
        fmt[i + 2] = ';';
    }
    fmt[fmt_len - 1] = 0;

    va_list args;
    va_start(args, n);
    vsprintf(s, fmt, args);
    va_end(args);

    return s;
}

char *ycsi_reset_sgr(char *s) {
    s = "0";
    return s;
}

void ycsi_apply(const char *s) {
    printf(s);
}
