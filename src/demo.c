#include <stdio.h>
#include <ycsi.h>

int main() {
    char csi_buf[255] = "\0";
    char sgr_buf[255] = "\0";

    ycsi_apply(ycsi_sgr(csi_buf, ycsi_build_sgr(sgr_buf, 3,
                                                YCSI_SGR_HIGH_INTENSITY,
                                                YCSI_SGR_FG_BLUE,
                                                YCSI_SGR_BG_GREEN)));

    printf("Hello, World!\n");

    ycsi_apply(ycsi_sgr(csi_buf, ycsi_build_sgr(sgr_buf, 3,
                                                YCSI_SGR_LOW_INTENSITY,
                                                YCSI_SGR_FG_BLUE,
                                                YCSI_SGR_BG_GREEN)));

    printf("Hello, World!\n");

    ycsi_apply(ycsi_sgr(csi_buf, ycsi_reset_sgr(sgr_buf)));

    printf("Hello, World!\n");

    return 0;
}