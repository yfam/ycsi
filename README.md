# YCSI - CSI Sequences Builder Library
##### Provides helper function to build CSI (Control Sequence Introducer) to manipulate console (terminal) output that support *ANSI escape codes*, including: _Move/Set console color_, _Set console font_, _Set background/foreground color_,...
More details about ASNI escape codes: [Wikipedia](https://en.wikipedia.org/wiki/ANSI_escape_code)

### Link to your app

Use static lib:

- Add ```include``` folder to your include directories
- Link ```ysci``` lib to your application

```cmake
add_executable(demo src/demo.c)

target_include_directories(demo PUBLIC include)

target_link_libraries(demo ycsi)
```


Or just include those source file in your project :D

### How to use

Almost functions generate corresponding CSI strings, print those sequences to standard output, your terminal (if supported) will automatically detect CSI syntax and manipulate the output.

| Function  | Effect             |
|-----------|-----------         |
| ```ycsi_cuu```  | Cursor up          |
| ```ycsi_cud```  | Cursor down        |
| ```ycsi_cuf```  | Cursor forward     |
| ```ycsi_cub```  | Cursor back        |
| ```ycsi_cup```  | Cursor position    |
| ```ycsi_sgr```  | Set sgr            |
| ```ycsi_ed```   | Erase display      |
| ...             | ...                |

```cpp
char csi_buf[255] = "\0";

printf(ycsi_cud(5)); // move cursor down 5 rows

std::cout << ycsi_cuf(5); // move cursor right 5 columns

ycsi_apply(ycsi_ed(2)); // erase entire console
```

### SGR Builder

SGR is the most popular features of CSI, used to manipulate console text color and effect.
Use ```ycsi_sgr``` and ```ycsi_build_sgr``` to generate CSI SGR sequences.

```cpp
char csi_buf[255] = "\0";
char sgr_buf[255] = "\0";
    
ycsi_apply(ycsi_sgr(csi_buf, ycsi_build_sgr(sgr_buf, 3,
                                            YCSI_SGR_HIGH_INTENSITY,
                                            YCSI_SGR_FG_BLUE,
                                            YCSI_SGR_BG_GREEN)));

printf("Hello, World!\n");

ycsi_apply(ycsi_sgr(csi_buf, ycsi_build_sgr(sgr_buf, 3,
                                            YCSI_SGR_LOW_INTENSITY,
                                            YCSI_SGR_FG_BLUE,
                                            YCSI_SGR_BG_GREEN)));

printf("Hello, World!\n");

ycsi_apply(ycsi_sgr(csi_buf, ycsi_reset_sgr(sgr_buf)));

printf("Hello, World!\n");

```